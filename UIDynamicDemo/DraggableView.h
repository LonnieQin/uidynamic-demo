//
//  DraggableView.h
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DraggableView : UIView<NSCopying>
- (instancetype) initWithFrame:(CGRect)frame animator:(UIDynamicAnimator*) animator;
@end
