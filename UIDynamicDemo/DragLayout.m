//
//  DragLayout.m
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DragLayout.h"
@interface DragLayout()
@property (nonatomic) NSIndexPath * indexPath;
@property (nonatomic) UIDynamicAnimator * animator;
@property (nonatomic) UIAttachmentBehavior * behavior;
@end
@implementation DragLayout
- (void) startDragginIndexPath:(NSIndexPath *)indexPath fromPoint:(CGPoint)p
{
    NSLog(@"%s",__func__);
    self.indexPath = indexPath;
    self.animator = [[UIDynamicAnimator alloc] initWithCollectionViewLayout:self];
    UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForItemAtIndexPath:self.indexPath];
    attributes.zIndex += 1;
    self.behavior = [[UIAttachmentBehavior alloc] initWithItem:attributes attachedToAnchor:p];
    self.behavior.length = 0;
    self.behavior.frequency = 10;
    [self.animator addBehavior:self.behavior];
    
    UIDynamicItemBehavior * dynamicItem = [[UIDynamicItemBehavior alloc] initWithItems:@[attributes]];
    dynamicItem.resistance = 10;
    [self.animator addBehavior:dynamicItem];
    [self updateDragLocation:p];
}

- (void) updateDragLocation:(CGPoint)point
{
    NSLog(@"%s",__func__);
    self.behavior.anchorPoint = point;
}

- (void)  stopDragging
{
    NSLog(@"%s",__func__);
    UICollectionViewLayoutAttributes * attributes = [super layoutAttributesForItemAtIndexPath:self.indexPath];
    [self updateDragLocation:attributes.center];
    self.indexPath = nil;
    self.behavior = nil;
}

- (NSArray*) layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray * existingAttributes = [super layoutAttributesForElementsInRect:rect];
 
    NSMutableArray * allatributes = [NSMutableArray new];
    for (UICollectionViewLayoutAttributes * a in existingAttributes) {
        if (![a.indexPath isEqual:self.indexPath]) {
            [allatributes addObject:a];
        }
    }
    [allatributes addObjectsFromArray:[self.animator itemsInRect:rect]];
    return allatributes;
}
@end
