//
//  TearOffBehavior.m
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "TearOffBehavior.h"
#import "DraggableView.h"
@implementation TearOffBehavior
- (instancetype) initWithDraggableView:(DraggableView *)view anchor:(CGPoint)anchor handler:(TearOffHandler)handler
{
    self = [super init];
    if (self) {
        _active = YES;
        
        [self addChildBehavior:[[UISnapBehavior alloc] initWithItem:view snapToPoint:anchor]];
        CGFloat distance = MIN(CGRectGetWidth(view.bounds), CGRectGetHeight(view.bounds));
        
        TearOffBehavior * __weak weakSelf = self;
        self.action = ^{
            TearOffBehavior * strongSelf = weakSelf;
            if (!PointsAreWithinDistance(view.center, anchor, distance)) {
                if (strongSelf.active) {
                    DraggableView * newView = [view copy];
                    [view.superview addSubview:newView];
                    TearOffBehavior * newTearOff = [[[strongSelf class] alloc] initWithDraggableView:newView anchor:anchor handler:handler];
                    newTearOff.active = NO;
                    [strongSelf.dynamicAnimator addBehavior:newTearOff];
                    handler(view,newView);
                    [strongSelf.dynamicAnimator removeBehavior:strongSelf];
                }
            } else {
                strongSelf.active = YES;
            }
        };
    }
    return self;
}
BOOL PointsAreWithinDistance(CGPoint p1,CGPoint p2,CGFloat distance)
{
    CGFloat dx = p1.x - p2.x;
    CGFloat dy = p1.y - p2.y;
    CGFloat currentDistance = hypotf(dx, dy);
    return (currentDistance < distance);
}
@end
