//
//  DefaultBehavior.h
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefaultBehavior : UIDynamicBehavior
- (void) addItem:(id<UIDynamicItem>) item;
- (void) removeItem:(id<UIDynamicItem>) item;
@end
