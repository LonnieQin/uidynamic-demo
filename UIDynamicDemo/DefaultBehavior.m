//
//  DefaultBehavior.m
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DefaultBehavior.h"

@implementation DefaultBehavior
- (instancetype) init
{
    self = [super init];
    if (self) {
        UICollisionBehavior * collisionBehavior = [UICollisionBehavior new];
        collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
        [self addChildBehavior:collisionBehavior];
        
        UIGravityBehavior * gravityBehavior = [UIGravityBehavior new];
        [self addChildBehavior:gravityBehavior];
    }
    return self;
}

- (void) addItem:(id<UIDynamicItem>)item
{
    for (id behavior in self.childBehaviors) {
        [behavior addItem:item];
    }
}

- (void) removeItem:(id<UIDynamicItem>)item
{
    for (id behavior in self.childBehaviors) {
        [behavior removeItem:item];
    }
}

@end
