//
//  DynamicsViewController.m
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DynamicsViewController.h"
//const CGPoint kInitialPoint1 = {200,300};
//const CGPoint kInitialPoint2 = {400,300};
CGPoint  kInitialPoint1;
CGPoint kInitialPoint2;
@interface DynamicsViewController ()
@property (weak, nonatomic) IBOutlet UIView *box1;
@property (weak, nonatomic) IBOutlet UIView *box2;
@property (nonatomic) UIDynamicAnimator * dynamicAnimator;
@end

@implementation DynamicsViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.dynamicAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    kInitialPoint1 = self.box1.center;
    kInitialPoint2 = self.box2.center;
    
}

- (IBAction)reset:(id)sender {
    [self.dynamicAnimator removeAllBehaviors];
    self.box1.center = kInitialPoint1;
    self.box1.transform = CGAffineTransformIdentity;
    self.box2.center = kInitialPoint2;
    self.box2.transform = CGAffineTransformIdentity;
}

- (IBAction)snap:(id)sender {
    CGPoint point = [self randomPoint];
    UISnapBehavior * snap = [[UISnapBehavior alloc] initWithItem:self.box1 snapToPoint:point];
    snap.damping = 0.25;
    [self addTemporaryBehavior:snap];
}

- (IBAction)attach:(id)sender {
    CGRect bounds = self.box1.bounds;
    CGFloat width = CGRectGetWidth(bounds);
    CGFloat height = CGRectGetHeight(bounds);
    
    CGFloat offsetWidth = width/2;
    CGFloat offsetHeight = height/2;
    UIOffset offsetUL = UIOffsetMake(-offsetWidth, -offsetHeight);
    UIOffset offsetUR = UIOffsetMake(offsetWidth, -offsetHeight);
    UIOffset offsetLL = UIOffsetMake(-offsetWidth, offsetHeight);
    UIOffset offsetLR = UIOffsetMake(offsetWidth, offsetHeight);
    
    CGFloat angleWidth = width/2;
    CGFloat angleHeight = height/2;
    
    CGPoint center = self.box1.center;
    CGPoint angleUL = CGPointMake(center.x-angleWidth,center.y-angleHeight);
    CGPoint angleUR = CGPointMake(center.x+angleWidth,center.y-angleHeight);
    CGPoint angleLL = CGPointMake(center.x-angleWidth,center.y+angleHeight);
    CGPoint angleLR = CGPointMake(center.x+angleWidth,center.y+angleHeight);
    
    [self.dynamicAnimator addBehavior:[[UIAttachmentBehavior alloc] initWithItem:self.box1 offsetFromCenter:offsetUL attachedToAnchor:angleUL]];
    [self.dynamicAnimator addBehavior:[[UIAttachmentBehavior alloc] initWithItem:self.box1 offsetFromCenter:offsetUR attachedToAnchor:angleUR]];
    [self.dynamicAnimator addBehavior:[[UIAttachmentBehavior alloc] initWithItem:self.box1 offsetFromCenter:offsetLL attachedToAnchor:angleLL]];
    [self.dynamicAnimator addBehavior:[[UIAttachmentBehavior alloc] initWithItem:self.box1 offsetFromCenter:offsetLR attachedToAnchor:angleLR]];
    UIAttachmentBehavior * attach2 = [[UIAttachmentBehavior alloc] initWithItem:self.box2 attachedToItem:self.box1];
    [self.dynamicAnimator addBehavior:attach2];
    UIPushBehavior * push = [[UIPushBehavior alloc] initWithItems:@[self.box2] mode:UIPushBehaviorModeInstantaneous];
    push.pushDirection = CGVectorMake(0, 2);
    [self.dynamicAnimator addBehavior:push];
}

- (IBAction)push:(id)sender {
    UIPushBehavior * push = [[UIPushBehavior alloc] initWithItems:@[self.box1] mode:UIPushBehaviorModeContinuous];
    push.pushDirection = CGVectorMake(1, 1);
    [self.dynamicAnimator addBehavior:push];
}

- (IBAction)gravity:(id)sender {
    UIGravityBehavior * gravity = [[UIGravityBehavior alloc] initWithItems:@[self.box1,self.box2]];
    gravity.action = ^{
        NSLog(@"%@",NSStringFromCGPoint(self.box1.center));
    };
    [self.dynamicAnimator addBehavior:gravity];
}

- (IBAction)collision:(id)sender {
    UICollisionBehavior * collision = [[UICollisionBehavior alloc] initWithItems:@[self.box1,self.box2]];
    [self.dynamicAnimator addBehavior:collision];
    UIPushBehavior * push  = [[UIPushBehavior alloc] initWithItems:@[self.box1] mode:UIPushBehaviorModeInstantaneous];
    push.pushDirection = CGVectorMake(3, 0);
    [self addTemporaryBehavior:push];
}

- (void) addTemporaryBehavior:(UIDynamicBehavior*) behavior
{
    [self.dynamicAnimator addBehavior:behavior];
    [self.dynamicAnimator performSelector:@selector(removeBehavior:) withObject:behavior afterDelay:1];
}

- (CGPoint) randomPoint
{
    CGSize size = self.view.bounds.size;
    return CGPointMake(arc4random_uniform(size.width), arc4random_uniform(size.height));
}

@end
