//
//  TearOffBehavior.h
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DraggableView;
typedef void(^TearOffHandler) (DraggableView * tornView,DraggableView * newPinView);
@interface TearOffBehavior : UIDynamicBehavior
@property (nonatomic) BOOL active;

- (instancetype) initWithDraggableView:(DraggableView*) view anchor:(CGPoint) anchor handler:(TearOffHandler) handler;
@end
