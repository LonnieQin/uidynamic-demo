//
//  DragLayout.h
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DragLayout : UICollectionViewFlowLayout
- (void) startDragginIndexPath:(NSIndexPath*) indexPath fromPoint:(CGPoint) p;
- (void) updateDragLocation:(CGPoint) point;
- (void) stopDragging;
@end
