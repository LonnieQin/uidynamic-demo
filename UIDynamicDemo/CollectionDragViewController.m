//
//  CollectionDragViewController.m
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "CollectionDragViewController.h"
#import "DragLayout.h"
@interface CollectionDragViewController ()

@end

@implementation CollectionDragViewController

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 100;
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    return cell;
}
- (IBAction)longPress:(UIGestureRecognizer*)sender {
    NSLog(@"%s",__func__);
    DragLayout * dragLayout = (DragLayout*)self.collectionView.collectionViewLayout;
    CGPoint location = [sender locationInView:self.collectionView];
    NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:location];
    UICollectionViewCell * cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    UIGestureRecognizerState state = sender.state;
    if (state == UIGestureRecognizerStateBegan) {
        [UIView animateWithDuration:0.25 animations:^{
            cell.backgroundColor = [UIColor redColor];
        }];
        [dragLayout startDragginIndexPath:indexPath fromPoint:location];
    } else if (state == UIGestureRecognizerStateEnded || state == UIGestureRecognizerStateCancelled) {
        [UIView animateWithDuration:0.25 animations:^{
            cell.backgroundColor = [UIColor lightGrayColor];
        }];
        [dragLayout stopDragging];
    } else {
        [dragLayout updateDragLocation:location ];
    }
}

@end
