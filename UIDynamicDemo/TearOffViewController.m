//
//  TearOffViewController.m
//  UIDynamicDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "TearOffViewController.h"
#import "TearOffBehavior.h"
#import "DefaultBehavior.h"
#import "DraggableView.h"
const CGFloat kShapeDimension = 100.0;
const NSUInteger kSliceCount = 6;
@interface TearOffViewController ()
@property (nonatomic) UIDynamicAnimator * animator;
@property (nonatomic) DefaultBehavior * defaultBehavior;
@end

@implementation TearOffViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    CGRect frame = CGRectMake(0, 0, kShapeDimension, kShapeDimension);
    DraggableView * dragView = [[DraggableView alloc] initWithFrame:frame animator:self.animator];
    dragView.center = CGPointMake(0.5*CGRectGetMidX(self.view.frame), 0.5*CGRectGetMidY(self.view.frame));
    dragView.alpha = 0.5;
    [self.view addSubview:dragView];
    
    DefaultBehavior * defaultBehavior = [DefaultBehavior new];
    [self.animator addBehavior:defaultBehavior];
    self.defaultBehavior = defaultBehavior;
    
    TearOffBehavior * tearOffBehavior = [[TearOffBehavior alloc] initWithDraggableView:dragView anchor:dragView.center handler:^(DraggableView *tornView, DraggableView *newPinView) {
        tornView.alpha = 1;
        [defaultBehavior addItem:tornView];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(trash:)];
        tap.numberOfTapsRequired = 2;
        [tornView addGestureRecognizer:tap];
    }];
    [self.animator addBehavior:tearOffBehavior];
    
}

- (void) trash:(UIGestureRecognizer*) g
{
    UIView * view = g.view;
    NSArray * subviews = [self sliceView:view intoRows:kSliceCount columns:kSliceCount];
    UIDynamicAnimator * trashAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    DefaultBehavior * defaultBehavior = [DefaultBehavior new];
    for (UIView * subview in subviews ) {
        [self.view addSubview:subview];
        [defaultBehavior addItem:subview];
        
        UIPushBehavior * push = [[UIPushBehavior alloc] initWithItems:@[subview] mode:UIPushBehaviorModeInstantaneous];
        [push setPushDirection:CGVectorMake((float)rand()/RAND_MAX - 0.5, (float)rand()/RAND_MAX-0.5)];
        [trashAnimator addBehavior:push];
        
        
        [UIView animateWithDuration:1 animations:^{
            subview.alpha = 0;
        } completion:^(BOOL finished) {
            [subview removeFromSuperview];
            [trashAnimator removeBehavior:push];
        }];
    }
    [self.defaultBehavior removeItem:view];
    [view removeFromSuperview];
}

- (NSArray*) sliceView:(UIView*) view intoRows:(NSUInteger) rows columns:(NSUInteger) columns
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    CGImageRef image = [UIGraphicsGetImageFromCurrentImageContext() CGImage];
    UIGraphicsEndImageContext();
    NSMutableArray * views = [NSMutableArray new];
    CGFloat width = CGImageGetWidth(image);
    CGFloat height = CGImageGetHeight(image);
    for (NSUInteger row = 0; row < rows; ++row) {
        for (NSUInteger column = 0; column < columns; ++column) {
            CGRect rect = CGRectMake(column * (width / columns), row * (height / rows), width/columns, height/rows);
            CGImageRef subimage = CGImageCreateWithImageInRect(image, rect);
            UIImageView * imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithCGImage:subimage]];
            CGImageRelease(subimage);
            subimage = NULL;
            imageView.frame = CGRectOffset(rect, CGRectGetMinX(view.frame), CGRectGetMinY(view.frame));
            [views addObject:imageView];
        }
    }
    return views;
}

@end
